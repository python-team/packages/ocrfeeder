<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="automaticrecognition" xml:lang="de">

<info>
    <link type="guide" xref="index#recognition"/>
    <link type="seealso" xref="addingimage"/>
    <desc>Ein Bild automatisch erkennen</desc>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2016-2017</mal:years>
    </mal:credit>
  </info>

<title>Automatische Erkennung</title>

<p><app>OCRFeeder</app> versucht den Inhalt eines Dokumentabbilds zu erkennen, wobei zwischen Bild- und Textbereichen unterschieden wird. Um dieses Konzept vereinfacht zu beschreiben, nennen wir es Erkennung.</p>

<p>Nach dem Hinzufügen eines Bildes können Sie den Inhalt automatisch erkennen lassen, indem Sie <guiseq><gui>Dokument</gui><gui>Dokument erkennen</gui></guiseq> auswählen.</p>

<note style="important"><p>Da das Layout eines Dokuments sehr vielfältig sein kann, kann die automatische Erkennung, insbesondere die optimale Teilung der Seite, zu inakzeptablen Ergebnissen für Ihr Dokument führen. In diesem Fall wird es erforderlich sein, die Ergebnisse der Erkennung manuell weiter zu bearbeiten.</p></note>

<note style="warning"><p>Durch die automatische Erkennung werden einige komplexe Vorgänge ausgeführt. Abhängig von der Größe des Bildes und der Komplexität des Layouts kann dies einige Zeit in Anspruch nehmen.</p>
<p>Die automatische Erkennung ersetzt alle Inhaltsbereiche der aktuell ausgewählten Seite.</p></note>

</page>
