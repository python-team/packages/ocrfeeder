<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="unpaper" xml:lang="gl">

<info>
    <link type="guide" xref="index#configuration"/>
    <link type="seealso" xref="manualeditionandcorrection"/>
    <desc>Limpar imaxes antes de executar o OCR</desc>
</info>

<title>Unpaper</title>

<p><em>Unpaper</em> é unha ferramenta para limpar imaxes, co fin de facelas máis doadas de ler na pantalla. Está dirixido principalmente a imaxes obtidas a partir de documentos dixitalizados, que xeralmente presentan po, marxes en negro ou outros fallos.</p>

<p><app>OCRFeeder</app> pode usar <em>Unpaper</em> para limpar as súasimaxes antes de procesalas, o que xeralmente ten como resultado un mellor recoñecemento.</p>

<p><em>Unpaper</em> debe estar instalado para usalo. Se non está instalado, <app>OCRFeeder</app> non mostrará as súas acción na interface.</p>

<p>Para usar <em>Unpaper</em> nunha imaxe cargada, prema en <guiseq><gui>Ferramentas</gui><gui>Unpaper</gui></guiseq>. Mostrarase o diálogo <gui>Procesador de imaxe Unpaper</gui> coas opcións de <em>Unpaper</em> e unha área para ver os cambios antes de aplicalos á imaxe cargada. Dependendo do tamaño e as características da imaxe, o uso desta ferramenta pode levar algún tempo.</p>

<p><em>Unpaper</em> pódese configurar abrindo <guiseq><gui>Editar</gui><gui>Preferencias</gui></guiseq> e accedendo á lapela <gui>Ferramentas</gui>. Nesta área pode introducir a ruta para o executable de <em>Unpaper</em> (normalmente este xa está configurado se  <em>Unpaper</em> foi instalado a primeira vez que <app>OCRFeeder</app> foi executado). Na mesma área, debaixo de  <gui>Pre-procesamento de imaxe</gui>, pode comprobar <gui>Imaxes Unpaper</gui> para facer que as imaxes sexan procesadas automaticamente por <em>Unpaper</em> despois de seren cargados en <app>OCRFeeder</app>. As opcións tomadas polo <em>Unpaper</em> cando se chama automaticamente despois de engadir unha imaxe pódense configurar premendo no botón <gui>Preferencias de Unpaper</gui>.</p>

</page>
